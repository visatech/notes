package com.visa.noteapp.dashboard.di;

import com.visa.noteapp.dashboard.activity.DashboardActivity;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by vikrantjoshi on 28/03/18.
 */

@Subcomponent(modules = MainActivityModule.class)
public interface MainActivityComponent extends AndroidInjector<DashboardActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<DashboardActivity>{}
}