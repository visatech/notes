package com.visa.noteapp.dashboard.di;

import com.visa.noteapp.data.network.ApiClass;
import com.visa.noteapp.data.network.NetworkManager;

import dagger.Module;
import dagger.Provides;

/**
 * Created by vikrantjoshi on 28/03/18.
 */

@Module
public class MainActivityModule {

    @Provides
    ApiClass getMeClass(NetworkManager manager){
        return new ApiClass(manager);
    }

}
