package com.visa.noteapp.dashboard.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.visa.noteapp.R;
import com.visa.noteapp.dashboard.interfaces.OnItemClickListener;
import com.visa.noteapp.models.NoteModel;

import java.util.List;

/**
 * Created by vikrantjoshi on 27/03/18.
 */

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.ListViewHolder> {

    private List<NoteModel> arrNotes;
    private OnItemClickListener onItemClickListener;

    public NoteListAdapter(List<NoteModel> arrNotes, OnItemClickListener onItemClickListener) {
        this.arrNotes = arrNotes;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View childView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notes_list, null);
        return new ListViewHolder(childView);
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {
        holder.tvNote.setText(arrNotes.get(position).getNote());
        holder.bindListener(arrNotes.get(position), onItemClickListener);

        /*
        * To show internet icon at top if note is downloaded from internet. Else hide the icon
        * */
        if (arrNotes.get(position).getIsFromServer().equalsIgnoreCase("Y")) {
            holder.ivWeb.setVisibility(View.VISIBLE);
        } else {
            holder.ivWeb.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return arrNotes.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        private View parentView;
        private TextView tvNote;
        private ImageView ivWeb;

        public ListViewHolder(View view) {
            super(view);
            parentView = view;
            initViews(view);
        }

        private void initViews(View view) {
            tvNote = view.findViewById(R.id.tvNote);
            ivWeb = view.findViewById(R.id.ivWeb);
        }

        private void bindListener(final NoteModel note, final OnItemClickListener onItemClickListener) {
            parentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(note);
                }
            });
        }
    }
}
