package com.visa.noteapp.dashboard.interfaces;

import com.visa.noteapp.models.NoteModel;

/**
 * Created by vikrantjoshi on 28/03/18.
 */

public interface OnItemClickListener {
    void onItemClick(NoteModel note);
}
