package com.visa.noteapp.dashboard.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.visa.noteapp.R;
import com.visa.noteapp.addnote.activity.AddNoteActivity;
import com.visa.noteapp.dashboard.adapters.NoteListAdapter;
import com.visa.noteapp.dashboard.interfaces.OnItemClickListener;
import com.visa.noteapp.dashboard.viewmodel.NoteViewModel;
import com.visa.noteapp.data.network.ApiClass;
import com.visa.noteapp.data.network.INetworkCallback;
import com.visa.noteapp.models.NoteModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

import static com.visa.noteapp.addnote.activity.AddNoteActivity.NOTE_DATA_KEY;

public class DashboardActivity extends AppCompatActivity implements INetworkCallback, OnItemClickListener {
    private RecyclerView rvNotesList;
    private NoteListAdapter noteListAdapter;
    private List<NoteModel> arrNotes = new ArrayList<>();
    private NoteViewModel noteViewModel;

    /*
    * Below class is injected using Dagger2
    * */
    @Inject
    ApiClass networkApiClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initFloatingButton();
        initViews();
        initViewModel();
        getDataFromNetwork();
        getData();
    }

    /*
    * Below method to write action on click of FAB.
    * */
    private void initFloatingButton() {
        FloatingActionButton fab = findViewById(R.id.fabAddNote);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                * Opening new activity on click of FAB
                * */
                Intent intent = new Intent(DashboardActivity.this, AddNoteActivity.class);
                startActivity(intent);
            }
        });
    }


    /*
    * Initializing views
    * */
    private void initViews() {
        rvNotesList = findViewById(R.id.rvNotesList);

        /*
        * Below method will initialize adapter, set layout manager to recycleview
        * and will set adapter to recycle view
        * */
        initAndSetAdapterToRecyclerView();
    }


    /*
    * Initializing view Model
    * */
    private void initViewModel() {
        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
    }


    /*
    * Initializing Adapter and setting it to Recycler View
    * */
    private void initAndSetAdapterToRecyclerView() {
        int NUMBER_OF_COLUMNS = 2;
        noteListAdapter = new NoteListAdapter(arrNotes, this);
        rvNotesList.setLayoutManager(new GridLayoutManager(this, NUMBER_OF_COLUMNS));
        rvNotesList.setAdapter(noteListAdapter);
    }


    /*
    * Get data from network
    * */
    private void getDataFromNetwork() {
        networkApiClass.getNotesDataFromNetwork(this);
    }


    /*
    * Get data from local database
    * */
    private void getData() {
        noteViewModel.getNoteList().observe(this, new Observer<List<NoteModel>>() {
            @Override
            public void onChanged(@Nullable List<NoteModel> noteModels) {
                arrNotes.clear();
                if (noteModels != null) {
                    arrNotes.addAll(noteModels);
                }
                noteListAdapter.notifyDataSetChanged();
            }
        });
    }


    /*
    * This method is fired from Network Manager, once the operation is done
    * */
    @Override
    public void onNetworkDataReceived(List<NoteModel> arrNoteData) {
        noteViewModel.insertNoteData(null, arrNoteData);
    }


    /*
    * This method will get called from adapter when view is clicked
    * */
    @Override
    public void onItemClick(NoteModel note) {
        Intent intent = new Intent(DashboardActivity.this, AddNoteActivity.class);
        intent.putExtra(NOTE_DATA_KEY, note);
        startActivity(intent);
    }
}

