package com.visa.noteapp.dashboard.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.visa.noteapp.data.backgroundtask.NoteDatabaseTask;
import com.visa.noteapp.data.backgroundtask.interfaces.INoteDatabaseListener;
import com.visa.noteapp.data.database.NoteDatabase;
import com.visa.noteapp.models.NoteModel;

import java.util.List;

/**
 * Created by vikrantjoshi on 27/03/18.
 */

public class NoteViewModel extends AndroidViewModel {
    private NoteDatabase database;
    private LiveData<List<NoteModel>> noteLiveData;

    /*
    * Passed application for context
    * */
    public NoteViewModel(Application application) {
        super(application);

        /*
        * Created instance of NoteDatabase class
        * */
        database = NoteDatabase.getInstance(application);


        /*
        * Below statement will get all notes data present in database
        * */
        noteLiveData = database.getNoteDao().getAllNotes();
    }

    /*
    * This will return Note data which is prefilled in constructor
    * */
    public LiveData<List<NoteModel>> getNoteList() {
        return noteLiveData;
    }


    /*
    * Insert data in database
    * */
    public void insertNoteData(INoteDatabaseListener iNoteDatabaseListener, List<NoteModel> noteModel) {

        /*
        * Custom class for Async Database operations
        * */
        NoteDatabaseTask databaseTask = new NoteDatabaseTask();


        /*
        * Converting list to array before inserting it into database
        * */
        NoteModel[] noteModelArray = noteModel.toArray(new NoteModel[noteModel.size()]);


        /*
        * This will insert data in database
        * */
        databaseTask.insertNoteData(database, iNoteDatabaseListener, noteModelArray);
    }
}
