package com.visa.noteapp.addnote.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;

import com.visa.noteapp.data.backgroundtask.NoteDatabaseTask;
import com.visa.noteapp.data.backgroundtask.interfaces.INoteDatabaseListener;
import com.visa.noteapp.data.database.NoteDatabase;
import com.visa.noteapp.models.NoteModel;

/**
 * Created by vikrantjoshi on 27/03/18.
 */

public class AddNoteViewModel extends AndroidViewModel {
    private NoteDatabase database;

    /*
    * Passed application for context
    * */
    public AddNoteViewModel(Application application) {
        super(application);

        /*
        * Created instance of NoteDatabase class
        * */
        database = NoteDatabase.getInstance(application);
    }

    /*
    * Insert data in database
    * */
    public void insertNoteData(INoteDatabaseListener iNoteDatabaseListener, NoteModel noteModel) {

        /*
        * Custom class for Async Database operations
        * */
        NoteDatabaseTask databaseTask = new NoteDatabaseTask();

        /*
        * This will insert data in database
        * */
        databaseTask.insertNoteData(database, iNoteDatabaseListener, noteModel);
    }
}
