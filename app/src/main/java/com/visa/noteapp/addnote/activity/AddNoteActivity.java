package com.visa.noteapp.addnote.activity;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.visa.noteapp.R;
import com.visa.noteapp.addnote.viewmodel.AddNoteViewModel;
import com.visa.noteapp.common.Utils;
import com.visa.noteapp.data.backgroundtask.interfaces.INoteDatabaseListener;
import com.visa.noteapp.models.NoteModel;

public class AddNoteActivity extends AppCompatActivity implements INoteDatabaseListener {
    public static final String NOTE_DATA_KEY = "NOTE_DATA_KEY";

    private EditText etNote;
    private TextView tvCharacterCount;
    private TextView tvSave;
    private TextView tvDate;

    private AddNoteViewModel noteViewModel;
    private NoteModel note = null;
    private AlertDialog.Builder alertDialogbuilder;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        getIntentData();
        initViews();
        initViewModel();
    }

    private void getIntentData() {
        note = (NoteModel) getIntent().getSerializableExtra(NOTE_DATA_KEY);

        if (note == null) {
            note = new NoteModel();
        }
    }

    @Override
    public void onBackPressed() {
        /*
        * If user has typed and not saved, confirm user whether to cancel
        * */
        if (!isServerNote() && !isNoteEmpty() && isNoteEditted()) {
            showConfirmationDialog();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.cancel();
            alertDialog.dismiss();
        }
    }

    /*
    * Initializing views
    * */
    private void initViews() {
        etNote = findViewById(R.id.etNote);
        tvCharacterCount = findViewById(R.id.tvCharacterCount);
        tvSave = findViewById(R.id.tvSave);
        tvDate = findViewById(R.id.tvDate);

        addTextChangeListener();
        prefillData();
    }


    /*
    * If note is editted, Edittext will be prefilled
    * */
    private void prefillData() {
        if (note != null) {
            etNote.setText(note.getNote());
            etNote.setSelection(note.getNote().length());
            if(note.getTimeStamp().length() > 0) {
                tvDate.setText("Last updated on " + Utils.getDateTimeFromTimeStamp(Long.parseLong(note.getTimeStamp())));
            }
            else {
                tvDate.setText("");
            }
            if (isServerNote()) {
                Toast.makeText(this, "You can only view this note as it is downloaded from server.", Toast.LENGTH_SHORT).show();
                etNote.setEnabled(false);
                tvSave.setVisibility(View.GONE);
            }
        }
    }


    /*
    * Initializing view Model
    * */
    private void initViewModel() {
        noteViewModel = ViewModelProviders.of(this).get(AddNoteViewModel.class);
    }


    /*
    * Added this listener to show character count while typing
    * */
    private void addTextChangeListener() {
        etNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                /*
                * Set count of characters
                * */
                tvCharacterCount.setText(s.length() + "/3000");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    /*
    * Check if Text field is empty or not for validation
    * */
    private boolean isNoteEmpty() {
        if (etNote.getText().toString().trim().length() > 0) {
            return false;
        }

        return true;
    }


    /*
    * Check if editted note is from server. If yes, then donot allow user to edit it.
    * */
    private boolean isServerNote() {
        if (note != null && note.getIsFromServer().equalsIgnoreCase("Y")) {
            return true;
        }
        return false;
    }


    /*
    * Check if note is editted. This is to confirm while back pressed whether to save note or not
    * */
    private boolean isNoteEditted() {
        if (note != null && !note.getNote().equals(etNote.getText().toString())) {
            return true;
        }
        return false;
    }


    /*
    * When user clicks on Save, this method will get called
    * */
    public void saveNote(View view) {
        if (!isNoteEmpty()) {

            /*
            * Binding data in model
            * */
            if (note == null) {
                note = new NoteModel();
            }
            note.setNote(etNote.getText().toString());
            note.setTimeStamp(Long.toString(Utils.getCurrentTimestamp()));


            /*
            * Insert note data into Database
            * */
            noteViewModel.insertNoteData(this, note);
        }else{
            Toast.makeText(this, "Blank note cannot be saved", Toast.LENGTH_SHORT).show();
        }
    }


    /*
    * This method is fired from Database Task, once the operation is done
    * */
    @Override
    public void onNoteDataSaved() {
        /*
        * Once data is saved, below message will be shown
        * */
        finish();
    }


    /*
    * Below dialog will be shown if their is any change in text and user press back button
    * */
    private void showConfirmationDialog() {
        alertDialogbuilder = new AlertDialog.Builder(this);
        alertDialogbuilder.setTitle(getResources().getString(R.string.back_press_dialog_title))
                .setMessage(getResources().getString(R.string.back_press_dialog_message));

        alertDialogbuilder.setPositiveButton(getResources().getString(R.string.back_press_dialog_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
                alertDialog.dismiss();
                finish();
            }
        });


        alertDialogbuilder.setNegativeButton(getResources().getString(R.string.back_press_dialog_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
                alertDialog.dismiss();
            }
        });
        alertDialog = alertDialogbuilder.create();
        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }
}
