package com.visa.noteapp.di;

import android.app.Application;
import android.content.Context;

import com.visa.noteapp.dashboard.di.MainActivityComponent;
import com.visa.noteapp.data.network.NetworkManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by vikrantjoshi on 28/03/18.
 */

@Module(subcomponents = {MainActivityComponent.class})
public class AppModule {

    @Provides
    @Singleton
    Context getContext(Application application) {
        return application;
    }


    @Provides
    @Singleton
    NetworkManager provideNetworkmanager(Context context) {
        return new NetworkManager(context);
    }
}



