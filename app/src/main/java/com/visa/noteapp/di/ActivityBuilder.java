package com.visa.noteapp.di;

import android.app.Activity;

import com.visa.noteapp.dashboard.activity.DashboardActivity;
import com.visa.noteapp.dashboard.di.MainActivityComponent;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

@Module
public abstract class ActivityBuilder {

    @Binds
    @IntoMap
    @ActivityKey(DashboardActivity.class)
    abstract AndroidInjector.Factory<? extends Activity> bindMainActivity(MainActivityComponent.Builder builder);


}