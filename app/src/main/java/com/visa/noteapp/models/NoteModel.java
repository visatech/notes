package com.visa.noteapp.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.visa.noteapp.common.Utils;

import java.io.Serializable;

/**
 * Created by vikrantjoshi on 27/03/18.
 */

@Entity
public class NoteModel implements Serializable {
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("note")
    @Expose
    private String note;

    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;

    @SerializedName("isFromServer")
    @Expose
    private String isFromServer = "N";

    public NoteModel() {
        id = (int) Utils.getCurrentTimestamp();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNote() {
        if (note == null)
            note = "";
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTimeStamp() {
        if (timeStamp == null)
            timeStamp = "";
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getIsFromServer() {
        return isFromServer;
    }

    public void setIsFromServer(String isFromServer) {
        this.isFromServer = isFromServer;
    }
}
