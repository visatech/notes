package com.visa.noteapp.data.network;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.visa.noteapp.models.NoteModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vikrantjoshi on 28/03/18.
 */

public class NetworkManager {
    private RetroInterface retroInterface;

    public NetworkManager(Context context) {
        initRetrofit(context);
    }

    private void initRetrofit(Context context) {
        Gson gson = new GsonBuilder().setLenient().create();

        /*
        * Below client created to add moc interceptor
        * */
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new OfflineMockInterceptor(context))
                .build();


        /*
        * Retorfit object created. Base url is moc url here.
        * */
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.visatech.shiksha/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        /*
        * Retro Api interface instance created here to make network calls in future.
        * */
        retroInterface = retrofit.create(RetroInterface.class);
    }

    /*
    * Below method to get all notes from network(Now moc data is returned)
    * */
    public void getNotesDataFromNetwork(final INetworkCallback iNetworkCallback) {
        Call<List<NoteModel>> call = retroInterface.getNotes();
        call.enqueue(new Callback<List<NoteModel>>() {
            @Override
            public void onResponse(Call<List<NoteModel>> call, retrofit2.Response<List<NoteModel>> response) {
                if (response != null && response.body() != null) {
                    /*
                    * Sending data to calling activity using interface
                    * */
                    iNetworkCallback.onNetworkDataReceived(response.body());
                } else {
                    /*
                    * Not worried incase of response or body null
                    * */
                }
            }

            @Override
            public void onFailure(Call<List<NoteModel>> call, Throwable t) {
                /*
                * Not worried incase of failure
                * */
            }
        });
    }


    /*
    * Below interceptor will moc file from assets for dummy data
    * */
    private class OfflineMockInterceptor implements Interceptor {
        private Context context;
        private final String ASSETS_FILE_NAME = "offlinenote.json";
        private final MediaType MEDIA_JSON = MediaType.parse("application/json");

        OfflineMockInterceptor(Context context) {
            this.context = context;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            InputStream stream = null;
            try {
                stream = context.getAssets().open(ASSETS_FILE_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
            /* Just read the file. */
            String fileData = parseStream(stream);

            /*
            * Created dummy response here
            * */
            Response response = new Response.Builder()
                    .body(ResponseBody.create(MEDIA_JSON, fileData))
                    .request(chain.request())
                    .message("") //No message required as of now
                    .protocol(Protocol.HTTP_2)
                    .code(200)
                    .build();

            return response;
        }

        /*
        * This method will parse data from file and return as string
        * */
        private String parseStream(InputStream stream) throws IOException {
            StringBuilder builder = new StringBuilder();
            BufferedReader in = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                builder.append(line);
            }
            in.close();
            return builder.toString();
        }
    }
}
