package com.visa.noteapp.data.network;

import javax.inject.Inject;

/**
 * Created by vikrantjoshi on 28/03/18.
 */

/*
* This class will be injected in activity
* */
public class ApiClass {

    private NetworkManager manager;

    @Inject
    public ApiClass(NetworkManager manager) {
        this.manager = manager;
    }

    /*
    * Below method is exposed publically to get data from network
    * */
    public void getNotesDataFromNetwork(final INetworkCallback iNetworkCallback) {
        manager.getNotesDataFromNetwork(iNetworkCallback);
    }
}
