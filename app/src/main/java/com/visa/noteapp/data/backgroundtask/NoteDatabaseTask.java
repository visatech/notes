package com.visa.noteapp.data.backgroundtask;

import android.os.AsyncTask;

import com.visa.noteapp.data.backgroundtask.interfaces.INoteDatabaseListener;
import com.visa.noteapp.data.database.NoteDatabase;
import com.visa.noteapp.models.NoteModel;


/**
 * Created by vikrantjoshi on 28/03/18.
 */

public class NoteDatabaseTask {

    /*
    * Publically exposed method to insert data
    * */
    public void insertNoteData(NoteDatabase database, INoteDatabaseListener iNoteDatabaseListener, NoteModel... noteModel) {
        /*
        * Executing background task here.
        * */
        new InsertNoteData(database, iNoteDatabaseListener, noteModel).execute();
    }

    /*
    * Background task for inserting data.
    * */
    private class InsertNoteData extends AsyncTask<Void, Void, Void> {
        private NoteDatabase database;
        private INoteDatabaseListener iNoteDatabaseListener;
        private NoteModel[] noteModel;

        /*
        * Constructor with required params
        * */
        InsertNoteData(NoteDatabase database, INoteDatabaseListener iNoteDatabaseListener, NoteModel... noteModel) {
            this.database = database;
            this.iNoteDatabaseListener = iNoteDatabaseListener;
            this.noteModel = noteModel;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            /*
            * Actual database task runs here. Not worried about return type so returning null.
            * */
            database.getNoteDao().insert(noteModel);
            return null;
        }

        @Override
        protected void onPostExecute(Void voidData) {
            super.onPostExecute(voidData);
            /*
            * If the callback is not null, calling class will be intimated of task done.
            * */
            if (iNoteDatabaseListener != null) {
                iNoteDatabaseListener.onNoteDataSaved();
            }
        }
    }
}
