package com.visa.noteapp.data.backgroundtask.interfaces;

/**
 * Created by vikrantjoshi on 28/03/18.
 */

/*
* Database operations callback
* */
public interface INoteDatabaseListener {
    void onNoteDataSaved();
}
