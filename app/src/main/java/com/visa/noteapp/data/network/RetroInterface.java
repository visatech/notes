package com.visa.noteapp.data.network;

import com.visa.noteapp.models.NoteModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by vikrantjoshi on 27/03/18.
 */

interface RetroInterface {
    /*
    * Below method will be used to call from Network Manager to get data from network
    * */
    @GET("/") //Dummy path/url added
    Call<List<NoteModel>> getNotes();
}
