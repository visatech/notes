package com.visa.noteapp.data.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.visa.noteapp.models.NoteModel;

import java.util.List;

/**
 * Created by vikrantjoshi on 28/03/18.
 */

/*
* Database operations in this class
* */
@Dao
public interface NoteDao {
    @Query("SELECT * FROM NoteModel")
    LiveData<List<NoteModel>> getAllNotes();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(NoteModel... noteModels);

    @Update
    void update(NoteModel... noteModels);

    @Delete
    void delete(NoteModel... noteModels);
}
