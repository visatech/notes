package com.visa.noteapp.data.network;

import com.visa.noteapp.models.NoteModel;

import java.util.List;

/**
 * Created by vikrantjoshi on 28/03/18.
 */

public interface INetworkCallback {

    void onNetworkDataReceived(List<NoteModel> arrNoteData);
}
