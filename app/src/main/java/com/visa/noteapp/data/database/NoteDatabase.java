package com.visa.noteapp.data.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.visa.noteapp.data.database.dao.NoteDao;
import com.visa.noteapp.models.NoteModel;

/**
 * Created by vikrantjoshi on 28/03/18.
 */

@Database(entities = {NoteModel.class}, version = 1)
public abstract class NoteDatabase extends RoomDatabase {

    private static final String DB_NAME = "noteDatabase.db";
    private static volatile NoteDatabase instance;

    public static synchronized NoteDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static NoteDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                NoteDatabase.class,
                DB_NAME).build();
    }

    public abstract NoteDao getNoteDao();
}