package com.visa.noteapp.common;

import com.visa.noteapp.di.AppComponent;
import com.visa.noteapp.di.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

/**
 * Created by vikrantjoshi on 28/03/18.
 */

public class AppApplication extends DaggerApplication {

    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }
}
