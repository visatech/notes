package com.visa.noteapp.common;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by vikrantjoshi on 28/03/18.
 */

public class Utils {

    private Utils() {
    }

    /*
    * Below method will return current timestamp
    * */
    public static long getCurrentTimestamp() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTimeInMillis();
    }

    public static String getDateTimeFromTimeStamp(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        Date currenTimeZone = calendar.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aaa");
        String displayTime = dateFormat.format(currenTimeZone) + " at " + timeFormat.format(currenTimeZone);

        return displayTime;
    }
}
